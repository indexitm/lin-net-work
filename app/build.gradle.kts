plugins {
    id("com.android.application")
}

android {
    namespace = "com.lin.linnetworkbase"
    compileSdk = 33

    defaultConfig {
        applicationId = "com.lin.linnetworkbase"
        minSdk = 19
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.8.0")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    //添加网络模块依赖
    implementation(project(mapOf("path" to ":network")))
    implementation ("com.squareup.retrofit2:retrofit:2.9.0")
    //图片加载框架
    implementation("com.github.bumptech.glide:glide:4.11.0")
    implementation("io.reactivex.rxjava2:rxandroid:2.1.1")
    implementation("io.reactivex.rxjava2:rxjava:2.2.12")
    implementation("androidx.preference:preference:1.0.0")
}