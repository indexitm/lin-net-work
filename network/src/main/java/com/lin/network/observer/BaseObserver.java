package com.lin.network.observer;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import android.database.Observable;

/**
 * 自定义Observer
 * @author llw
 */
public abstract class BaseObserver<T> implements Observer<T> {

    /**
     * 开始
     * @param d the Disposable instance whose {@link Disposable#dispose()} can
     * be called anytime to cancel the connection
     */
    @Override
    public void onSubscribe(Disposable d) {

    }

    /**
     * /继续
     * @param t
     *          the item emitted by the Observable
     */
    @Override
    public void onNext(T t) {
        onSuccess(t);
    }

    /**
     * 异常
     * @param e
     *          the exception encountered by the Observable
     */
    @Override
    public void onError(Throwable e) {
        onFailure(e);
    }

    /**
     * 完成
     */
    @Override
    public void onComplete() {

    }

    /**
     * 成功
     * @param t
     */
    public abstract void onSuccess(T t);

    /**
     * 失败
     * @param e
     */
    public abstract void onFailure(Throwable e);
}
