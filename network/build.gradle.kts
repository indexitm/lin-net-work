plugins {
    id("com.android.library")
}

android {
    namespace = "com.lin.network"
    compileSdk = 33

    defaultConfig {
        minSdk = 19

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {

    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.8.0")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    //retrofit2
     implementation("com.squareup.retrofit2:retrofit:2.4.0")
    //这里用api 是为了让其他模块也可以使用gson
     implementation("com.squareup.retrofit2:converter-gson:2.4.0")
    //日志拦截器
     implementation("com.squareup.okhttp3:logging-interceptor:3.9.0")
     implementation("com.squareup.retrofit2:adapter-rxjava2:2.4.0")
    //rxjava
     implementation("io.reactivex.rxjava2:rxandroid:2.1.1")
     implementation("io.reactivex.rxjava2:rxjava:2.2.12")
     implementation("androidx.preference:preference:1.0.0")
    //图片加载框架
     implementation("com.github.bumptech.glide:glide:4.11.0")
    annotationProcessor ("com.github.bumptech.glide:compiler:4.11.0")

}